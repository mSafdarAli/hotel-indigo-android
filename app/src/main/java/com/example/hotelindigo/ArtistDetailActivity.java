package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.Networks.OkHttpController;
import com.example.hotelindigo.customViews.ProgressDialogue;
import com.example.hotelindigo.helpers.DownloadImageTask;
import com.example.hotelindigo.listeners.OkHttpListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class ArtistDetailActivity extends AppCompatActivity implements OkHttpListener, View.OnClickListener {

    ProgressDialogue progressDialogue;
    OkHttpController okHttpController;

    TextView artistNameTV;
    ImageView artistIV;
    EditText artistDescriptionET;
    ImageButton audioPlayPauseBtn,backIB;
    String audioNote;
    SeekBar playerSeekBar;


    //MARK: - audio Things
    private MediaPlayer mediaPlayer;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_detail);
        progressDialogue = new ProgressDialogue(this);

        artistNameTV = findViewById(R.id.artistNameTV);
        artistIV = findViewById(R.id.artistIV);
        artistDescriptionET = findViewById(R.id.artistDescriptionET);

        playerSeekBar= findViewById(R.id.playerSeekBar);
        audioPlayPauseBtn = findViewById(R.id.audioPlayPauseBtn);
        backIB = findViewById(R.id.backIB);


        backIB.setOnClickListener(this);
        audioPlayPauseBtn.setOnClickListener(this);

        //Receive Data
        Bundle bundle = getIntent().getExtras();
        String artistID = bundle.getString("artistID");

        callArtistDetailAPI(artistID);

        //MARK: - Audio Things
        mediaPlayer = new MediaPlayer();
        playerSeekBar.setMax(100);




        playerSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                SeekBar seekBar = (SeekBar) view;
                int playerPosition = (mediaPlayer.getDuration() / 100) * seekBar.getProgress();
                mediaPlayer.seekTo(playerPosition);

                return false;
            }
        });

    }


    private void callArtistDetailAPI(String artistID) {

        if (Constants.IsNetworkAvailable(this)){

            String api,parameter,key;

            progressDialogue.show();
            JSONObject jsonObject = new JSONObject();

            api = APILINKS.ARTIST_DETAIL + artistID +"?filters&populate=Picture&populate=Audionote";
            parameter = jsonObject.toString();
            key = "ARTIST_DETAIL";

            okHttpController = new OkHttpController(this,api,key,"GET",parameter);
            okHttpController.execute();


        }else{
            Constants.MakeToast(this,"Please connect to internet and try again.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mediaPlayer.isPlaying()){
            handler.removeCallbacks(updater);
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            handler.removeCallbacks(updater);
            mediaPlayer.pause();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.audioPlayPauseBtn:
                if (mediaPlayer.isPlaying()){
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();
                    audioPlayPauseBtn.setBackground(getResources().getDrawable(R.drawable.playgreen));
                }else{
                    mediaPlayer.start();
                    audioPlayPauseBtn.setBackground(getResources().getDrawable(R.drawable.pausegreen));
                    updateSeekBar();
                }
            break;
            case R.id.backIB:

                if (mediaPlayer.isPlaying()){
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();
                    finish();
                }else{
                    finish();
                }

                break;
            default:
                Log.e("Default","default value");
        }
    }

    @Override
    public void OkHttpSuccess(String key, JSONObject result) {
        if (result != null && key != null){
            switch (key){
                case "ARTIST_DETAIL":

                    progressDialogue.dismiss();
                    artistDetailResponse(result);

                    break;
            }
        }

    }

    @Override
    public void OkHttpFailure(String result) {
        Log.e("OkHttpFailure",""+result);
        progressDialogue.dismiss();
    }

    private void artistDetailResponse(JSONObject result) {

        Log.e("Artist Detail Result",""+result);

//        Constants.MakeToast(this,"ARTIST Detail Fetched successfully.");

        try {

            String name = result.optJSONObject("data").optJSONObject("attributes").optString("Name");
            String description = result.optJSONObject("data").optJSONObject("attributes").optString("Description");

            artistNameTV.setText(name);
            artistDescriptionET.setText(description);

            try {
                audioNote = result.optJSONObject("data").optJSONObject("attributes").optJSONObject("Audionote").optJSONObject("data").optJSONObject("attributes").optString("url");
                String audioURL = APILINKS.IMAGE_BASE_URL + audioNote;
                prepareMediaPlayer(audioURL);

                Log.e("Audio Url = ",""+audioNote);
            }catch (Exception ex){
                audioNote = "";
            }

            String imageUrl = null;
            try {
                imageUrl = result.optJSONObject("data").optJSONObject("attributes").optJSONObject("Picture").optJSONObject("data").optJSONObject("attributes").optJSONObject("formats").optJSONObject("small").optString("url");
                new DownloadImageTask(artistIV).execute(APILINKS.IMAGE_BASE_URL+imageUrl);
            }catch (Exception ex){
                imageUrl = "";
                Log.e("No Image Url",""+ex);
            }

        }catch (Exception exception){
            Log.e("Exceptions error",""+exception);
        }

    }

    private  void  prepareMediaPlayer(String audioURL){
        try {

            Log.e("Artist Detail Audio",""+audioURL);
            mediaPlayer.setDataSource(audioURL);
            mediaPlayer.prepare();


        }catch (Exception e){
            Constants.MakeToast(this,""+e);
        }
    }

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateSeekBar();
            long currentDuration = mediaPlayer.getCurrentPosition();
//            textCurrentTime = milisecond

        }
    };

    private void updateSeekBar(){
        if (mediaPlayer.isPlaying()){

            playerSeekBar.setProgress((int) (((float)mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration()) * 100));
            handler.postDelayed(updater,1000);

        }
    }

}