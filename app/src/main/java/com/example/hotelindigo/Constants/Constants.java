package com.example.hotelindigo.Constants;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import org.json.JSONArray;


public class Constants {


        public static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        public  static JSONArray jsonArray;
        public  static JSONArray artsJsonArray;


        public static void alertDialoge(String title, String message, final Context context){
                AlertDialog.Builder a_Builder = new AlertDialog.Builder(context);
                a_Builder.setMessage(message).setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();


                        }
                });
                AlertDialog alert = a_Builder.create();
                alert.setTitle(title);
                alert.show();
        }


        public static boolean IsNetworkAvailable(Context context) {
                ConnectivityManager connectivityManager
                        = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                assert connectivityManager != null;
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }


        public static void MakeToast(final Context context, final String message) {
            ((Activity) context).runOnUiThread(new Runnable() {
             public void run() {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
             });
         }



}
