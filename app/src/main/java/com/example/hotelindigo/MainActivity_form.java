package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.Networks.OkHttpController;
import com.example.hotelindigo.customViews.ProgressDialogue;
import com.example.hotelindigo.helpers.SharedPref;
import com.example.hotelindigo.listeners.OkHttpListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class MainActivity_form extends AppCompatActivity implements OkHttpListener {

    Button submitButton;
    Button skipButton;
    ProgressDialogue progressDialogue;
    OkHttpController okHttpController;
    EditText nameTF,emailTF,cityTF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_form);

        skipButton = findViewById(R.id.skipForm);
        submitButton = findViewById(R.id.submit);
        progressDialogue = new ProgressDialogue(this);
        nameTF = findViewById(R.id.nameTF);
        emailTF = findViewById(R.id.emailTF);
        cityTF = findViewById(R.id.cityTF);

        onClick();
    }

    public void onClick() {

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(nameTF.length()==0)
                {
                    Constants.alertDialoge("Oops!","Please enter your name.", MainActivity_form.this);
//                    nameTF.requestFocus();
//                    nameTF.setError("Enter your name");
                }
                else if(emailTF.length()==0)
                {
                    Constants.alertDialoge("Oops!","Please enter your email.", MainActivity_form.this);

//                    emailTF.requestFocus();
//                    emailTF.setError("Enter your email");
                }
                else if(cityTF.length()==0)
                {
                    Constants.alertDialoge("Oops!","Please enter city name.", MainActivity_form.this);
//                    cityTF.requestFocus();
//                    cityTF.setError("Enter your city");
                }
                else
                {
                    String email = emailTF.getText().toString().trim();
                    if (email.matches(Constants.emailPattern)){
                        callAPI();
//                        startActivity();
                    }else{
                        Constants.alertDialoge("Oops!","Please enter valid email address.", MainActivity_form.this);
                    }

                }
            }
        });

    }

    private void callAPI(){

        if (Constants.IsNetworkAvailable(MainActivity_form.this)){
            String api,parameter,key;
            try {
                progressDialogue.show();
                String android_id = UUID.randomUUID().toString();

                SharedPref.setString(this,"uuid",android_id);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Name",nameTF.getText().toString().trim());
                jsonObject.put("Email",emailTF.getText().toString().trim());
                jsonObject.put("City",cityTF.getText().toString().trim());
                jsonObject.put("device_id",android_id);
                jsonObject.put("ud_id",android_id);
                JSONObject data = new JSONObject();
                data.put("data",jsonObject);

                api = APILINKS.CLIENTS_URL;
                parameter = data.toString();
                key = "USER_FORM";

                okHttpController = new OkHttpController(this,api,key,"POST",parameter);
                okHttpController.execute();

            }catch (JSONException e){
                e.printStackTrace();
                progressDialogue.dismiss();
                Constants.MakeToast(this,"Failed to add user data.");
            }

        }else {
            Constants.MakeToast(this,"Please connect to internet and try again.");
        }

    }



    private void startActivity(){
        SharedPref.setBoolean(MainActivity_form.this,"isLoggedIn",true);
        Intent gotoOurStoryActivity = new  Intent(MainActivity_form.this,OurStoryActivity.class);
        startActivity(gotoOurStoryActivity);
    }

    @Override
    public void OkHttpSuccess(String key, JSONObject result) {
        if (result != null && key != null){
            switch (key){
                case "USER_FORM":

                    progressDialogue.dismiss();
                    userFormResponse(result);
                    break;
            }
        }
    }

    @Override
    public void OkHttpFailure(String result) {
        Log.e("OkHttpFailure",""+result);
        progressDialogue.dismiss();
    }

    private void userFormResponse(JSONObject result){
        Log.e("Result",""+result);
        startActivity();
    }
}