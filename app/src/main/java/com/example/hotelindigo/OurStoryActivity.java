package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class OurStoryActivity extends AppCompatActivity {


    ImageButton playBtn;
    Button skipBtn;
    TextView ourStoryText;
    VideoView videoView;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_our_story);

        playBtn = findViewById(R.id.playBtn);
        skipBtn = findViewById(R.id.skipBtn);
        ourStoryText = findViewById(R.id.ourStoryText);
        videoView = findViewById(R.id.videoView);
        videoView.setVisibility(View.GONE); //hide video view as it is on top of all widgets

        onClick();
    }
    public void onClick() {

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide view and play video
                hideViews();
                playVideo();
            }
        });
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoView.stopPlayback();
                Intent gotoTabBarActivity = new  Intent(OurStoryActivity.this,BottomNavigation.class);
                startActivity(gotoTabBarActivity);
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                showViews();
            }
        });

    }

    private void playVideo() {

        videoView.setVisibility(View.VISIBLE);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        mediaController.setVisibility(View.INVISIBLE);
        videoView.setMediaController(mediaController); //it hides video play pause button and shows full screen video
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/raw/"+"introductionvideo"));
        videoView.requestFocus(); //it will remove the video bottom navigation.
        videoView.start();


    }

    private void hideViews(){
        ourStoryText.setVisibility(View.INVISIBLE);
        playBtn.setVisibility(View.GONE);

    }
    private void showViews(){
        ourStoryText.setVisibility(View.VISIBLE);
        playBtn.setVisibility(View.VISIBLE);
        videoView.setVisibility(View.GONE);
    }
}