package com.example.hotelindigo.Models;

public class RoomsListModel {

    String roomImageURL;
    String roomTypeName;


    public RoomsListModel(String roomImageURL, String roomTypeName) {
        this.roomImageURL = roomImageURL;
        this.roomTypeName = roomTypeName;
    }

    public String getRoomImageURL() {
        return roomImageURL;
    }

    public void setRoomImageURL(String roomImageURL) {
        this.roomImageURL = roomImageURL;
    }

    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }




}
