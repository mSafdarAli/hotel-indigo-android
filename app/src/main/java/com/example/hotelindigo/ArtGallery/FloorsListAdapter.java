package com.example.hotelindigo.ArtGallery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.renderscript.Sampler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelindigo.OnFloorClickListener;
import com.example.hotelindigo.R;

import java.util.ArrayList;
import java.util.List;

public class FloorsListAdapter extends RecyclerView.Adapter<FloorsListAdapter.viewHolder> {

    ArrayList<FloorModel> floorsList;
    Context context;
    private final OnFloorClickListener floorClickListener;

    int selectedPosition;//selected floor position holder

    public FloorsListAdapter(ArrayList<FloorModel> floorsList,Context context,OnFloorClickListener floorClickListener) {
        this.floorsList = floorsList;
        this.floorClickListener = floorClickListener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.floors_list_item_layout,parent,false);

        return new viewHolder(view);
    }


    @SuppressLint({"ResourceAsColor", "RecyclerView"})
    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position)
    {

        holder.floorText.setText(floorsList.get(position).getNumber());

        if (selectedPosition == position ){
            holder.floorText.setBackgroundResource(R.drawable.selected_floor_background);
            holder.floorText.setTextColor(Color.WHITE);
        }else{
            holder.floorText.setBackgroundResource(R.drawable.floors_corner_radius);
            holder.floorText.setTextColor(R.color.DarkBlue);
        }


        holder.floorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("floor ID",""+floorsList.get(position).getId());

                holder.floorText.setBackgroundResource(R.drawable.selected_floor_background);
                holder.floorText.setTextColor(Color.WHITE);
                selectedPosition = position;//getting position if item to change color
                notifyDataSetChanged(); //it is the native method using itself as notification to recycler view no need to define it anywhere.
                //MARK: - Passing floor model object to my floor click listener
                floorClickListener.onFloorClick(floorsList.get(position));

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return floorsList.size();
    }

    class viewHolder extends RecyclerView.ViewHolder
    {
        TextView floorText;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            floorText = itemView.findViewById(R.id.floorTextID);
        }
    }

}
