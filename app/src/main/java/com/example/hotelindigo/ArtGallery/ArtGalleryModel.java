package com.example.hotelindigo.ArtGallery;

public class ArtGalleryModel {
    String floorName;

    public ArtGalleryModel(String floorName) {
        this.floorName = floorName;
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }
}
