package com.example.hotelindigo.ArtGallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelindigo.ArtDetailActivity;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.OurRoom.RoomDetailActivity;
import com.example.hotelindigo.R;
import com.example.hotelindigo.helpers.DownloadImageTask;

import java.util.ArrayList;

public class ArtsAdapter extends RecyclerView.Adapter<ArtsAdapter.ArtsViewHolder> {

    ArrayList <ArtsModel>imagesUrlList;
    Context context;



    public ArtsAdapter(ArrayList<ArtsModel> imagesUrlList,Context context){

        this.imagesUrlList = imagesUrlList;
        this.context = context;
    }

    @NonNull
    @Override
    public ArtsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.arts_images_item,parent,false);


        return new ArtsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ArtsViewHolder holder, int position) {


        String imageUri = APILINKS.IMAGE_BASE_URL + imagesUrlList.get(position).getImageUrl();


        new DownloadImageTask(holder.artsIV).execute(imageUri);
        holder.artsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("hello","image clicked");

                Intent intent = new Intent(context, ArtDetailActivity.class);

                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("artID", ""+imagesUrlList.get(position).getArtId());

                intent.putExtras(bundle);

                context.startActivity(intent);



            }
        });

    }

    @Override
    public int getItemCount() {
        return this.imagesUrlList.size();
    }

    public  class ArtsViewHolder extends RecyclerView.ViewHolder{
        ImageView artsIV;

        public ArtsViewHolder(@NonNull View itemView) {
            super(itemView);

            artsIV = itemView.findViewById(R.id.artsIV);

        }

    }
}
