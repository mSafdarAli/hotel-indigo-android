package com.example.hotelindigo.ArtGallery;

public class ArtsModel {
    int artId;
    String imageUrl;

    public ArtsModel(int artId, String imageUrl) {
        this.artId = artId;
        this.imageUrl = imageUrl;
    }


    public int getArtId() {
        return artId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setArtId(int artId) {
        this.artId = artId;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }




}
