package com.example.hotelindigo.ArtGallery;

public class ArtDetailModel {

    String artImageUrl,imageName,description,audioNote;

    public ArtDetailModel(String artImageUrl, String imageName, String description, String audioNote) {

        this.artImageUrl = artImageUrl;
        this.imageName = imageName;
        this.description = description;
        this.audioNote = audioNote;

    }

    public void setArtImageUrl(String artImageUrl) {
        this.artImageUrl = artImageUrl;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAudioNote(String audioNote) {
        this.audioNote = audioNote;
    }

    public String getArtImageUrl() {
        return artImageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public String getDescription() {
        return description;
    }

    public String getAudioNote() {
        return audioNote;
    }

}
