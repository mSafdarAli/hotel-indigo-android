package com.example.hotelindigo.ArtGallery;

public class FloorModel {
    int id;
    String Number;
    String Collection_name;

    public FloorModel(int id, String number, String collection_name) {
        this.id = id;
        Number = number;
        Collection_name = collection_name;
    }
    public int getId() {
        return id;
    }

    public String getNumber() {
        return Number;
    }

    public String getCollection_name() {
        return Collection_name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public void setCollection_name(String collection_name) {
        Collection_name = collection_name;
    }

}
