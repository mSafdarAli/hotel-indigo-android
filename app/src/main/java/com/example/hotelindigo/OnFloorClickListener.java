package com.example.hotelindigo;

import com.example.hotelindigo.ArtGallery.FloorModel;

public interface OnFloorClickListener {

    void onFloorClick(FloorModel model);

}
