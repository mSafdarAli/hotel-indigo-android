package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Welcom2 extends AppCompatActivity implements View.OnClickListener {

    Button next1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcom2);

        next1 = findViewById(R.id.next1);
        next1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.next1:
                startActivity();

                break;
            default:
                Log.e("E Tag","Hello default");

        }
    }

    public void startActivity(){
        Intent i = new  Intent(Welcom2.this,Welcome3.class);
        startActivity(i);
    }



}