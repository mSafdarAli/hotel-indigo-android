package com.example.hotelindigo.customViews;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;


import com.example.hotelindigo.R;

import java.util.Objects;

public class ProgressDialogue extends AlertDialog {
    public ProgressDialogue(Context context) {
        super(context);
        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    @Override
    public void show() {
        super.show();
        setContentView(R.layout.dialog_progress);
    }
}
