package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.OurRoom.RoomDetailActivity;
import com.example.hotelindigo.helpers.SharedPref;

public class MainActivity extends AppCompatActivity  {


    ProgressBar progressBar;
    Button bNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.splashProgressbar);
        progressBar.setProgress(0);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (SharedPref.getBoolean(MainActivity.this,"isLoggedIn",false)){
                    Log.e("Login=","`hello");
                    Intent gotoBottomNavScr = new Intent(MainActivity.this,BottomNavigation.class);
                    startActivity(gotoBottomNavScr);
                }else{
                    Intent gotoWelcome = new Intent(MainActivity.this,Welcom2.class);
                    startActivity(gotoWelcome);
                }

            }
        }, 1500);

    }
}

