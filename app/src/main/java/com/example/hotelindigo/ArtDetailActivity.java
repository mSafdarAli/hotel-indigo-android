package com.example.hotelindigo;

import static com.example.hotelindigo.R.*;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.Networks.OkHttpController;
import com.example.hotelindigo.customViews.ProgressDialogue;
import com.example.hotelindigo.helpers.DownloadImageTask;
import com.example.hotelindigo.listeners.OkHttpListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class ArtDetailActivity extends AppCompatActivity implements OkHttpListener, View.OnClickListener {


    ImageButton audioPlayPauseBtn,backIB;
    SeekBar playerSeekbar;
    ImageView artIV;
    TextView artNameTV,artistNameTV;
    EditText artDetailET;
    ProgressDialogue progressDialogue;
    OkHttpController okHttpController;

    String audioNote;
    String artID;

    //MARK: - audio Things
    private MediaPlayer mediaPlayer;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_art_detail);


        audioPlayPauseBtn = findViewById(id.audioPlayPauseBtn);
        backIB = findViewById(R.id.backIB);
        playerSeekbar = findViewById(id.playerSeekbar);
        artIV = findViewById(id.artIV);
        artNameTV = findViewById(id.artNameTV);
        artistNameTV = findViewById(id.artistNameTV);
        artDetailET = findViewById(id.artDetailET);

        backIB.setOnClickListener(this);
        audioPlayPauseBtn.setOnClickListener(this);
        artistNameTV.setOnClickListener(this);
        progressDialogue = new ProgressDialogue(this);

        Bundle bundle = getIntent().getExtras();
        artID = bundle.getString("artID");


        Log.e("selected Art ID",""+artID);
        callArtDetailAPI(artID);//131

        //MARK: - Audio Things
        mediaPlayer = new MediaPlayer();
        playerSeekbar.setMax(100);


//        prepareMediaPlayer();
        playerSeekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                SeekBar seekBar = (SeekBar) view;
                int playerPosition = (mediaPlayer.getDuration() / 100) * seekBar.getProgress();
                mediaPlayer.seekTo(playerPosition);

                return false;
            }
        });



    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mediaPlayer.isPlaying()){
            handler.removeCallbacks(updater);
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            handler.removeCallbacks(updater);
            mediaPlayer.pause();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){


            case R.id.audioPlayPauseBtn:

                Log.e("playClick:","playButton");
                if (mediaPlayer.isPlaying()){

                    audioPlayPauseBtn.setBackground(getResources().getDrawable(drawable.playgreen));
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();

                }else{
                    mediaPlayer.start();
                    audioPlayPauseBtn.setBackground(getResources().getDrawable(drawable.pausegreen));
                    updateSeekBar();

                }
                break;

            case R.id.artistNameTV:

                Log.e("Tag = ","artist Name click.");

                Intent gotoArtistDetail = new Intent(this,ArtistDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("artistID",artistID);
                gotoArtistDetail.putExtras(bundle);

                startActivity(gotoArtistDetail);

                break;

                case R.id.backIB:

                    if (mediaPlayer.isPlaying()){
                        handler.removeCallbacks(updater);
                        mediaPlayer.pause();
                        finish();

                    }else{
                        finish();
                    }

                break;



            default:

                Log.e("Tag = ","default");

        }

    }

    private void callArtDetailAPI(String artDetail) {

        if (Constants.IsNetworkAvailable(this)){

            try {
                String api,parameter,key;

                progressDialogue.show();
                JSONObject jsonObject = new JSONObject();

                api = APILINKS.ART_DETAIL + artDetail +"?filters&populate=Picture&populate=Audionote&populate=artist";
                parameter = jsonObject.toString();
                key = "ART_DETAIL";

                okHttpController = new OkHttpController(this,api,key,"GET",parameter);
                okHttpController.execute();
            }catch (Exception ex){
                Log.e("error","");
            }



        }else {
            Constants.MakeToast(this,"Please connect to internet and try again.");
        }
    }


    @Override
    public void OkHttpSuccess(String key, JSONObject result) {
        if (result != null && key != null){
            switch (key){
                case "ART_DETAIL":

                    progressDialogue.dismiss();
                    artDetailResponse(result);
                    break;


            }
        }
    }


    @Override
    public void OkHttpFailure(String result) {
        Log.e("OkHttpFailure",""+result);
        progressDialogue.dismiss();
    }
    String artImageUrl = null,artName,artistName,description,artistID;
    private void artDetailResponse(JSONObject result){

        Log.e("Result Art Detail",""+result);
//        Constants.MakeToast(this,"Art Details Fetched successfully.");

        //JSON OBJECT IS GETTING HERE


        try {
            JSONObject dataObject = result.optJSONObject("data");

            artName = dataObject.optJSONObject("attributes").optString("Title");

            try {
                description = dataObject.optJSONObject("attributes").optString("Description");
            }catch (Exception ex){
                description = "No description found.";
                Log.e("Error","description null:"+ex);
            }

            artistName = dataObject.optJSONObject("attributes").optJSONObject("artist").optJSONObject("data").optJSONObject("attributes").optString("Name");
            artistID = dataObject.optJSONObject("attributes").optJSONObject("artist").optJSONObject("data").optString("id");

            try {
                audioNote = dataObject.optJSONObject("attributes").optJSONObject("Audionote").optJSONObject("data").optJSONObject("attributes").optString("url");

                String audioURL = APILINKS.IMAGE_BASE_URL + audioNote;
                prepareMediaPlayer(audioURL);
                Log.e("Art Detail Audio",""+audioNote);
            }catch (Exception ex){
                Log.e("error art detail",""+ex);
            }


            JSONArray pictureDataJsonArray = dataObject.optJSONObject("attributes").optJSONObject("Picture").optJSONArray("data");

            if (pictureDataJsonArray.length()>0){
                artImageUrl = pictureDataJsonArray.optJSONObject(0).optJSONObject("attributes").optJSONObject("formats").optJSONObject("small").optString("url");
            }

            for (int position = 0;position<pictureDataJsonArray.length();position++){

            }

            artNameTV.setText(artName);
            artistNameTV.setText(artistName);
            artDetailET.setText(description);
            new DownloadImageTask(artIV).execute(APILINKS.IMAGE_BASE_URL + artImageUrl);

        }catch (Exception ex){
            Log.e("Error"," while setting Art Detail Screen"+ex);
        }

    }
    private  void  prepareMediaPlayer(String audioURL){
        try {

            Log.e("ArtDetailAudio:",""+audioURL);
            mediaPlayer.setDataSource(audioURL);
            mediaPlayer.prepare();

        }catch (Exception e){
            Constants.MakeToast(this,""+e);
        }
    }

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateSeekBar();
            long currentDuration = mediaPlayer.getCurrentPosition();
//            textCurrentTime = milisecond

        }
    };

    private void updateSeekBar(){
        if (mediaPlayer.isPlaying()){
            playerSeekbar.setProgress((int) (((float)mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration()) * 100));
            handler.postDelayed(updater,1000);

        }
    }
}