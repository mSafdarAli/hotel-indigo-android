package com.example.hotelindigo.OurRoom;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.R;
import com.example.hotelindigo.helpers.DownloadImageTask;

import java.util.ArrayList;

public class RoomDetailAdapter extends RecyclerView.Adapter<RoomDetailAdapter.RoomDetailViewHolder> {


    private ArrayList<String> imagesUrlList = new ArrayList<String>();
    Context context;

    public RoomDetailAdapter(ArrayList imagesUrlList, Context context){
        this.imagesUrlList = imagesUrlList;
        this.context = context;
    }

    @NonNull
    @Override
    public RoomDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.room_images_item,parent,false);

        return new RoomDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomDetailViewHolder holder, int position) {

        holder.roomsImagesIV.setBackgroundResource(R.drawable.bathroom);
        String imageURL = APILINKS.IMAGE_BASE_URL + imagesUrlList.get(position);

        Log.e("Current Image URL",""+imageURL);

        new DownloadImageTask(holder.roomsImagesIV).execute(imageURL);

    }

    @Override
    public int getItemCount() {

        return imagesUrlList.size();
    }

    //manually created class (view holder class)
    public class RoomDetailViewHolder extends RecyclerView.ViewHolder{

        ImageView roomsImagesIV;

        public RoomDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            roomsImagesIV = itemView.findViewById(R.id.roomsImagesIV);
        }
    }
}
