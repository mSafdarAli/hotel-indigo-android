package com.example.hotelindigo.OurRoom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelindigo.Fragment.RoomsFragment;
import com.example.hotelindigo.Help1Activity;
import com.example.hotelindigo.Models.RoomsListModel;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.R;
import com.example.hotelindigo.helpers.DownloadImageTask;

import java.util.ArrayList;

public class OurRoomsAdapter extends RecyclerView.Adapter<OurRoomsAdapter.OurRoomsViewHolder> {


    ArrayList<RoomsListModel> list;
    Context context;

    public OurRoomsAdapter(ArrayList<RoomsListModel> list,Context context) {
        this.list = list;
        this.context = context;
    }



    @NonNull
    @Override
    public OurRoomsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.our_rooms_list_item_layout, parent, false);
        return new OurRoomsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OurRoomsViewHolder holder, @SuppressLint("RecyclerView") int position) {
        RoomsListModel model = list.get(position);

        holder.ourRoomName.setText(list.get(position).getRoomTypeName());
        String imageUri = APILINKS.IMAGE_BASE_URL + list.get(position).getRoomImageURL();

        Log.e("image url = ", ""+imageUri);
        new DownloadImageTask(holder.ourRoomImage).execute(imageUri);

        holder.CvItem.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, RoomDetailActivity.class);

                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("index", String.valueOf(position));

                intent.putExtras(bundle);

                context.startActivity(intent);


            }
        });
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public class OurRoomsViewHolder extends RecyclerView.ViewHolder{
        ImageView ourRoomImage;
        TextView ourRoomName;
        CardView CvItem;
//        View CvItem;

        public OurRoomsViewHolder(@NonNull View itemView) {
            super(itemView);
            ourRoomImage = itemView.findViewById(R.id.roomIV);
            ourRoomName = itemView.findViewById(R.id.roomNameTV);
            CvItem = itemView.findViewById(R.id.CvItem);
        }
    }
}
