package com.example.hotelindigo.OurRoom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.MainActivity_form;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.Networks.OkHttpController;
import com.example.hotelindigo.R;
import com.example.hotelindigo.customViews.ProgressDialogue;
import com.example.hotelindigo.helpers.SharedPref;
import com.example.hotelindigo.listeners.OkHttpListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

public class RoomDetailActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView roomImagesRV;
    Button backBtn,bookRoomBtn;
    ImageButton audioPlayPauseBtn;
    TextView roomNameTV;
    EditText roomDetailET;
    SeekBar playerSeekBar;
    int indexNumber;
    String audioUri,textCurrentTime;

    ProgressDialogue progressDialogue;

    String audioNote;
    private MediaPlayer mediaPlayer;
    private Handler handler = new Handler();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);

        roomImagesRV = findViewById(R.id.roomImagesRV);
        backBtn = findViewById(R.id.backBtn);
        bookRoomBtn = findViewById(R.id.bookRoomBtn);
        audioPlayPauseBtn = findViewById(R.id.audioPlayPauseBtn);
        roomNameTV = findViewById(R.id.roomNameTV);
        roomDetailET = findViewById(R.id.roomDetailET);
        playerSeekBar = findViewById(R.id.playerSeekbar);


        //RECYCLER VIEW THINGS
//        ArrayList<String> imagesUrlList = new ArrayList<String>();
//        imagesUrlList.add("a");
//        imagesUrlList.add("b");
//        imagesUrlList.add("c");

//        roomImagesRV.setAdapter(new RoomDetailAdapter(hello,this));
//        roomImagesRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        roomImagesRV.setAdapter(new RoomDetailAdapter(imagesUrlList,this));


        Bundle bundle = getIntent().getExtras();
        String index = bundle.getString("index");
        indexNumber = Integer.parseInt(index);
        setData(indexNumber); //initilizing the data


        backBtn.setOnClickListener(this);
        bookRoomBtn.setOnClickListener(this);
        audioPlayPauseBtn.setOnClickListener(this);
        progressDialogue = new ProgressDialogue(this);


        mediaPlayer = new MediaPlayer();
        playerSeekBar.setMax(100);

        Log.e("Selected Index","="+indexNumber);


        //AUDIO THINGS
//        prepareMediaPlayer();
        playerSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                SeekBar seekBar = (SeekBar) view;
                int playerPosition = (mediaPlayer.getDuration() / 100) * seekBar.getProgress();
                mediaPlayer.seekTo(playerPosition);

                return false;
            }
        });

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mediaPlayer.isPlaying()){
            handler.removeCallbacks(updater);
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        prepareMediaPlayer("http://159.89.54.139:1337/uploads/Rooms_8d8ab56e14.mp3");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            handler.removeCallbacks(updater);
            mediaPlayer.pause();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtn:

                Log.e("Back","back button clicked");
//                finish();
                if (mediaPlayer.isPlaying()){
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();
                    finish();

                }else{
                    finish();
                }
                break;
            case  R.id.audioPlayPauseBtn:

                if (mediaPlayer.isPlaying()){

                    audioPlayPauseBtn.setBackground(getResources().getDrawable(R.drawable.playgreen));
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();

                }else{
                    mediaPlayer.start();
                    audioPlayPauseBtn.setBackground(getResources().getDrawable(R.drawable.pausegreen));
                    updateSeekBar();

                }
//                if (mediaPlayer.isPlaying()){
//                    handler.removeCallbacks(updater);
//                    mediaPlayer.pause();
//                    audioPlayPauseBtn.setImageResource(R.drawable.backbtn);
//                }else{
//                    mediaPlayer.start();
//                    audioPlayPauseBtn.setImageResource(R.drawable.ic_play_arrow);
//                    updateSeekBar();
//                }

                break;
            case R.id.bookRoomBtn:

                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.hotelindigo.com/hotels/gb/en/dubai/dxbbp/hoteldetail?fromRedirect=true&qSrt=sBR&qIta=99617851&icdv=99617851&glat=SEAR_PDSEA-_-G_F-IMEA_FS-MEA_H-IMEA_HS-UAE_IN_BRI_EXM_HOTEL-DXBBP&qSlH=DXBBP&setPMCookies=true&qSHBrC=IN&qDest=Marasi%20Drive,%20Business%20Bay,%20Dubai,%20AE&dp=true&gclid=Cj0KCQjwhLKUBhDiARIsAMaTLnGwqJFp-Vff-fn415n3sM1Ml6vQQ9Z4hBsOYYn0yQucp8-Bvw_7z58aAiwyEALw_wcB&cm_mmc=PDSEA-_-G_F-IMEA_FS-MEA_H-IMEA_HS-UAE_IN_BRI_EXM_HOTEL-DXBBP&srb_u=1"));
                startActivity(intent);

                break;
            default:
                Log.e("","switch default");
        }
    }

    private  void  prepareMediaPlayer(String audioURL){
        try {

            Log.e("ArtDetailAudio:",""+audioURL);
            mediaPlayer.setDataSource(audioURL);
            mediaPlayer.prepare();

        }catch (Exception e){
            Constants.MakeToast(this,""+e);
        }
    }


    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateSeekBar();
            long currentDuration = mediaPlayer.getCurrentPosition();
//            textCurrentTime = milisecond

        }
    };

    private void updateSeekBar(){
        if (mediaPlayer.isPlaying()){
            playerSeekBar.setProgress((int) (((float)mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration()) * 100));
            handler.postDelayed(updater,1000);

        }
    }


    private void setData(int indexNumber){
        if (Constants.jsonArray != null){

            try {
                roomNameTV.setText(Constants.jsonArray.optJSONObject(indexNumber).optJSONObject("attributes").optString("Type"));
                roomDetailET.setText(Constants.jsonArray.optJSONObject(indexNumber).optJSONObject("attributes").optString("Description"));
//                audioUri = ""+Constants.jsonArray.optJSONObject(indexNumber).optJSONObject("Audionote").optJSONObject("data").optJSONObject("attributes").optString("url");


                Log.e("Json Array count",""+Constants.jsonArray.length());
                JSONArray dataArray = Constants.jsonArray.optJSONObject(indexNumber).optJSONObject("attributes").optJSONObject("Images").optJSONArray("data");


                Log.e("ImagesArrayLength==",""+dataArray.length());

                ArrayList<String>  imagesUrlList = new ArrayList<String>(); //this array is storing room images urls.

                for (int position = 0;position <dataArray.length(); position++){

                    String imagesURL = dataArray.optJSONObject(position).optJSONObject("attributes").optString("url");//.optJSONObject("formats").optJSONObject("thumbnail").optString("url");
                    imagesUrlList.add(imagesURL);

                }
                Log.e("imagesUrlListCount==",""+imagesUrlList.size());

                roomImagesRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                roomImagesRV.setAdapter(new RoomDetailAdapter(imagesUrlList,this));
                SnapHelper snapHelper = new PagerSnapHelper();
                snapHelper.attachToRecyclerView(roomImagesRV);


                audioNote = "/uploads/Rooms_8d8ab56e14.mp3";
                String audioURL = APILINKS.IMAGE_BASE_URL + audioNote;
//                prepareMediaPlayer(audioURL);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

}