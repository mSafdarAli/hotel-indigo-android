package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Help1Activity extends AppCompatActivity {

    Button back;
    Button next3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help1);

        next3 = findViewById(R.id.help2);
        back = findViewById(R.id.back02);

        onClick();

        TextView textView = (TextView) findViewById(R.id.text02);
        String text = "<font color = #e37222> Scan </font> <font color = #37424a> the artworks to learn more about them </font>";
        textView.setText(Html.fromHtml(text));
    }
    public void onClick() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        next3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new  Intent(Help1Activity.this,Help2Activity.class);
                startActivity(intent2);
            }
        });
    }
}