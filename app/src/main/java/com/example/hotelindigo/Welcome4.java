package com.example.hotelindigo;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Welcome4 extends AppCompatActivity{

    Button back;
    Button next4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome4);

        back = findViewById(R.id.button3);
        next4 = findViewById(R.id.button4);

        onClick();

        TextView textView = (TextView) findViewById(R.id.textView5);
        String text = "<font color = #37424a> Some artworks will send you a </font> " + "<font color = #e37222> push notification </font> " +
                "<font color = #37424a>Tap the notification to learn more about the artwork! Just make sure to keep your phone's Bluetooth on.   </font>";
        textView.setText(Html.fromHtml(text));
    }

    private void onClick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        next4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new  Intent(Welcome4.this,MainActivity_form.class);
                startActivity(n);
            }
        });
    }
}