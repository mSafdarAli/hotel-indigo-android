package com.example.hotelindigo.Networks;

public class APILINKS {

//    private static String BASE_URL = "http://134.209.84.217:1337/api/";
//    public static String IMAGE_BASE_URL = "http://134.209.84.217:1337";

    public static int fragmentNumber = -1;

    //159.89.54.139
    private static String BASE_URL = "http://159.89.54.139:1337/api/";
    public static String IMAGE_BASE_URL = "http://159.89.54.139:1337";


    public static String CLIENTS_URL = BASE_URL + "clients"; //success
    public static String USER_DATA = BASE_URL + "clients/?filters[device_id][$eq]="; //success
    public static String ROOMS_LIST = BASE_URL + "rooms/?populate=Cover,Images&populate=Audionote";
    public static String FLOORS_LIST = BASE_URL + "floors";
    public static String ARTS_BY_FLOOR = BASE_URL + "artworks?filters[floor][id][$eq]=";
    public static String ART_DETAIL = BASE_URL + "artworks/";
    public static String ARTIST_NAME = BASE_URL + "artworks?filters[floor][id][$eq]=";
    public static String ARTIST_DETAIL = BASE_URL + "artists/";


}
