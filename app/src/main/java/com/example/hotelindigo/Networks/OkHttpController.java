package com.example.hotelindigo.Networks;

import android.os.AsyncTask;
import android.util.Log;


import com.example.hotelindigo.listeners.OkHttpListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Dany on 16-Aug-17.
 **/

public class OkHttpController extends AsyncTask<JSONObject, JSONObject, String> {
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client;
    private OkHttpListener okHttpListener;
    private String url, key, parameters, method;
    private boolean responseCheck = true;

    public OkHttpController(OkHttpListener okHttpListener, String url, String key, String method, String parameters) {
        this.okHttpListener = okHttpListener;
        this.url = url;
        this.key = key;
        this.parameters = parameters;
        this.method = method;
        client = new OkHttpClient().newBuilder()
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build();
    }

    @Override
    protected String doInBackground(JSONObject... jsonObjects) {
        try {
            Request request;
            RequestBody body;
            responseCheck = false;
            Log.e("OKHTTP_url", url);

            switch (method) {
                case "GET":
                    request = new Request.Builder()
                            .url(url)
                            .build();
                    break;
                case "POST":
                    Log.e("OKHTTP_Param", parameters);
                    body = RequestBody.create(JSON, parameters);
                    request = new Request.Builder()
                            .url(url)
                            .post(body)
                            .build();
                    break;
                case "PUT":
                    Log.e("OKHTTP_Param", parameters);
                    body = RequestBody.create(JSON, parameters);
                    request = new Request.Builder()
                            .url(url)
                            .put(body)
                            .build();
                    break;
                default:
//                    Delete Request
                    Log.e("OKHTTP_Param", parameters);
                    body = RequestBody.create(JSON, parameters);
                    request = new Request.Builder()
                            .url(url)
                            .delete(body)
                            .build();
                    break;

            }

            Response response = client.newCall(request).execute();
            String result = response.body().string();

            Log.e("OkHTTP_RESULT", result);



            switch (response.code()) {
                case 200:
                    Log.e("Status","200");
                case 201:
                    responseCheck = true;
                    return result;
                case 401:
                    return "Authentication Failed";
                case 404:
                    return "Data Not Found";
                default:
                    return "An error has occurred. Please try again";
            }

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            if (!responseCheck) {
                okHttpListener.OkHttpFailure(result + "\n" + url);
            } else {
                okHttpListener.OkHttpSuccess(key, (method.equals("GET") || method.equals("POST") ? new JSONObject(result) : new JSONObject()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            okHttpListener.OkHttpFailure(result);
        }
    }
}