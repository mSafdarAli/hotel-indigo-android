package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Help2Activity extends AppCompatActivity {

    Button back;
    Button close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help2);

        close = findViewById(R.id.helpClose);
        back = findViewById(R.id.back03);

        onClick();

        TextView textView = (TextView) findViewById(R.id.textView05);
        String text = "<font color = #37424a> Some artworks will send you a </font> " + "<font color = #e37222> push notification </font> " +
                "<font color = #37424a>Tap the notification to learn more about the artwork! Just make sure to keep your phone's Bluetooth on.   </font>";
        textView.setText(Html.fromHtml(text));
    }
    public void onClick() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new  Intent(Help2Activity.this,BottomNavigation.class);
                startActivity(intent2);
            }
        });
    }
}