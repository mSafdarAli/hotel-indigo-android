package com.example.hotelindigo.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hotelindigo.ArtGallery.ArtsAdapter;
import com.example.hotelindigo.ArtGallery.ArtsModel;
import com.example.hotelindigo.ArtGallery.FloorsListAdapter;
import com.example.hotelindigo.ArtGallery.ArtGalleryModel;
import com.example.hotelindigo.ArtGallery.FloorModel;
import com.example.hotelindigo.ArtistDetailActivity;
import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.Models.RoomsListModel;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.Networks.OkHttpController;
import com.example.hotelindigo.OnFloorClickListener;
import com.example.hotelindigo.OurRoom.RoomDetailActivity;
import com.example.hotelindigo.R;
import com.example.hotelindigo.customViews.ProgressDialogue;
import com.example.hotelindigo.listeners.OkHttpListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ArtGalleryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArtGalleryFragment extends Fragment implements OkHttpListener, View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //New
    RecyclerView floorsRV,artsRV;
    TextView artsByTV,collectionNameTV;
    ImageView informationIV;


    ArrayList<ArtGalleryModel> floorholder;

    ArrayList<FloorModel> floorsList;
    int artistID = -1; //using this artist id to get artist detail in artist detail activity

    ProgressDialogue progressDialogue;
    OkHttpController okHttpController;
    ArrayList<RoomsListModel> floorsListArray;

    public ArtGalleryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ArtGalleryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArtGalleryFragment newInstance(String param1, String param2) {
        ArtGalleryFragment fragment = new ArtGalleryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        progressDialogue = new ProgressDialogue(this.getContext());
        callFloorsAPI();
        callArtsAPI(1);
        callArtistNameAPI(1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_art_gallery, container, false);
        floorsRV = view.findViewById(R.id.floorRV);
        artsRV = view.findViewById(R.id.artsRV);

        artsByTV = view.findViewById(R.id.artsByTV);
        collectionNameTV = view.findViewById(R.id.collectionNameTV);
        informationIV = view.findViewById(R.id.informationIV);

        //actions initilizer
        informationIV.setOnClickListener(this);
        artsByTV.setOnClickListener(this);


        floorsRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        floorholder = new ArrayList<>();
        ArtGalleryModel obj1 = new ArtGalleryModel("Floor 1");
        floorholder.add(obj1);

        ArtGalleryModel obj2 = new ArtGalleryModel("Floor 2");
        floorholder.add(obj2);




        return view;
    }

    private void callArtistNameAPI(int floorID) {

        if (Constants.IsNetworkAvailable(this.getContext())){

            String api,parameter,key;

            progressDialogue.show();
            JSONObject jsonObject = new JSONObject();

            String floorIDString = String.valueOf(floorID);

            api = APILINKS.ARTIST_NAME + floorIDString +"&populate=floor&populate=floor.artists";
            parameter = jsonObject.toString();

            key = "ARTIST_NAME";

            okHttpController = new OkHttpController(this,api,key,"GET",parameter);
            okHttpController.execute();


        }else {
            Constants.MakeToast(this.getContext(),"Please connect to internet and try again.");
        }
    }



    private void callArtsAPI(int floorID) {

        if (Constants.IsNetworkAvailable(this.getContext())){

            String api,parameter,key;

            progressDialogue.show();
            JSONObject jsonObject = new JSONObject();

            String floorIDString = String.valueOf(floorID);

            api = APILINKS.ARTS_BY_FLOOR + floorIDString +"&populate=Picture";
            parameter = jsonObject.toString();
            key = "ARTS_LIST";

            okHttpController = new OkHttpController(this,api,key,"GET",parameter);
            okHttpController.execute();


        }else {
            Constants.MakeToast(this.getContext(),"Please connect to internet and try again.");
        }
    }

    private void callFloorsAPI() {

        if (Constants.IsNetworkAvailable(this.getContext())){

            String api,parameter,key;

            progressDialogue.show();
            JSONObject jsonObject = new JSONObject();

            api = APILINKS.FLOORS_LIST;
            parameter = jsonObject.toString();
            key = "FLOORS_LIST";

            okHttpController = new OkHttpController(this,api,key,"GET",parameter);
            okHttpController.execute();

        }else {
            Constants.MakeToast(this.getContext(),"Please connect to internet and try again.");
        }

    }

    @Override
    public void OkHttpSuccess(String key, JSONObject result) {
        if (result != null && key != null){
            switch (key){
                case "FLOORS_LIST":

                    progressDialogue.dismiss();
                    floorsListResponse(result);
                    break;
                case "ARTS_LIST":

                    progressDialogue.dismiss();
                    artsListResponse(result);
                    break;
                case "ARTIST_NAME":
                    progressDialogue.dismiss();
                    artistNameResponse(result);

                    break;



            }
        }
    }

    @Override
    public void OkHttpFailure(String result) {
        Log.e("OkHttpFailure",""+result);
        progressDialogue.dismiss();

    }

    //ARTIST
    private void artistNameResponse(JSONObject result){
        Log.e("Artist Name Result",""+result);

//        Constants.MakeToast(this.getContext(),"ARTIST Name Fetched successfully.");

        JSONArray dataArray = result.optJSONArray("data");
        Constants.artsJsonArray = dataArray;

        if (dataArray != null){

            for (int position = 0; position <dataArray.length(); position++ ){

                JSONArray artistsJsonArray = dataArray.optJSONObject(position).optJSONObject("attributes").optJSONObject("floor").optJSONObject("data").optJSONObject("attributes").optJSONObject("artists").optJSONArray("data");
                String collectionName = dataArray.optJSONObject(position).optJSONObject("attributes").optJSONObject("floor").optJSONObject("data").optJSONObject("attributes").optString("Collection_name").toString();
                Log.e("CollectionName",""+collectionName);
                collectionNameTV.setText(collectionName); //Collection Name

                for (int second = 0;second<artistsJsonArray.length();second++){

                    artistID = artistsJsonArray.optJSONObject(second).optInt("id"); //will use this to pass into artist detail acrtivity
                    String artistsName = artistsJsonArray.optJSONObject(second).optJSONObject("attributes").optString("Name");
                    artsByTV.setText("by " + artistsName);
                }
            }
        }
    }

    //ARTS
    private void artsListResponse(JSONObject result){
        Log.e("ARts List",""+result);
//        Constants.MakeToast(this.getContext(),"ARTS Fetched successfully.");

        JSONArray dataArray = result.optJSONArray("data");
        Constants.artsJsonArray = dataArray;

        ArrayList <ArtsModel> artsList = new ArrayList<>();


        if (dataArray != null){

            for (int position = 0; position <dataArray.length(); position++ ){

                int artID = dataArray.optJSONObject(position).optInt("id");
                JSONArray pictureDataArray = dataArray.optJSONObject(position).optJSONObject("attributes").optJSONObject("Picture").optJSONArray("data");

                for (int secondCount =0 ;secondCount<pictureDataArray.length();secondCount++){

                    String artImageUrl = pictureDataArray.optJSONObject(secondCount).optJSONObject("attributes").optJSONObject("formats").optJSONObject("small").optString("url");
                    ArtsModel obj = new ArtsModel(artID,artImageUrl);
                    artsList.add(obj);
                }
            }
            artsRV.setAdapter(new ArtsAdapter(artsList,this.getContext()));
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this.getContext(),2);
            artsRV.setLayoutManager(gridLayoutManager);

        }
    }


    //FLOORS
    private void floorsListResponse(JSONObject result){
        Log.e("Result Floors List",""+result);
//        Constants.MakeToast(this.getContext(),"Floors List Fetched successfully.");

        JSONArray dataArray = result.optJSONArray("data");
        Constants.jsonArray = dataArray;

        floorsList = new ArrayList<>(); //model type array list

       if (dataArray != null){

           for (int position = 0; position <dataArray.length(); position++ ){

                int floorID = dataArray.optJSONObject(position).optInt("id");
                String floorName = dataArray.optJSONObject(position).optJSONObject("attributes").optString("Number");
                String collectionName = dataArray.optJSONObject(position).optJSONObject("attributes").optString("Collection_name");
                FloorModel obj = new FloorModel(floorID,floorName,collectionName);
                floorsList.add(obj);


            }

           floorsRV.setAdapter(new FloorsListAdapter(floorsList, this.getContext(), new OnFloorClickListener() {
               @Override
               public void onFloorClick(FloorModel model) {
                   Log.e("Floors","OnFloorClickId:"+model.getNumber());
                   selectedFloor(model); //using fragment.

               }
           }));

       }

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.informationIV:
            case R.id.artsByTV:
                //open artist detail activity
                try {
                    //Create the bundle
                    if (artistID == -1){

                    }else{

                        Intent intent = new Intent(this.getContext(), ArtistDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("artistID", String.valueOf(artistID));
                        intent.putExtras(bundle);
                        this.startActivity(intent);

                    }
                }catch (Exception e){
                    Log.e("error",""+e);
                }
                break;

        }
    }
    private void selectedFloor(FloorModel model){

        callArtsAPI(model.getId());
        int floorId = model.getId();
        callArtistNameAPI(floorId);

    }

}