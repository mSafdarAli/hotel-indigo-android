package com.example.hotelindigo.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.Networks.OkHttpController;
import com.example.hotelindigo.OurStoryActivity;
import com.example.hotelindigo.R;
import com.example.hotelindigo.customViews.ProgressDialogue;
import com.example.hotelindigo.helpers.SharedPref;
import com.example.hotelindigo.listeners.OkHttpListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements OkHttpListener, View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    BottomNavigationView bottomNavigationView;
    LinearLayout socialLL;
    ProgressDialogue progressDialogue;
    OkHttpController okHttpController;
    TextView nameTV;
    ImageView galaxyV;
    ImageView scanIV;
    ImageButton facebookIB,instagramIB,tiktokIB,socialDotsIB;

    CardView cardView;
    CardView cardView4;
    CardView cardView3;
    CardView cardViewHelp;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        progressDialogue = new ProgressDialogue(this.getContext());
        callClientAPI();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        nameTV = (TextView) rootView.findViewById(R.id.nameTV);
        galaxyV = rootView.findViewById(R.id.galaxyV);
        scanIV = rootView.findViewById(R.id.scanIV);
        socialLL = rootView.findViewById(R.id.socialLL);
        tiktokIB = rootView.findViewById(R.id.tiktokIB);
        instagramIB = rootView.findViewById(R.id.instagramIB);
        facebookIB = rootView.findViewById(R.id.facebookIB);
        socialDotsIB = rootView.findViewById(R.id.socialDotsIB);
        cardView = rootView.findViewById(R.id.cardView);
        cardView4 = rootView.findViewById(R.id.cardView4);
        cardView3 = rootView.findViewById(R.id.cardView3);
        cardViewHelp = rootView.findViewById(R.id.cardViewHelp);

        final View navigationView = inflater.inflate(R.layout.activity_bottom_navigation,container,false);

        bottomNavigationView = navigationView.findViewById(R.id.buttomNavigation);

        cardView.setOnClickListener(this);
        cardView4.setOnClickListener(this);
        cardView3.setOnClickListener(this);
        cardViewHelp.setOnClickListener(this);

        tiktokIB.setOnClickListener(this);
        instagramIB.setOnClickListener(this);
        facebookIB.setOnClickListener(this);
        socialDotsIB.setOnClickListener(this);
        socialLL.setVisibility(View.INVISIBLE);

        /* Create Animation */
        Animation rotation = AnimationUtils.loadAnimation(this.getContext(), R.anim.button_rotation);
        rotation.setRepeatCount(Animation.INFINITE);

        /* start Animation */
        galaxyV.startAnimation(rotation);


        return rootView;
//        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    private void callClientAPI(){

        if (Constants.IsNetworkAvailable(this.getContext())){

            String api,parameter,key;

            progressDialogue.show();
            JSONObject jsonObject = new JSONObject();

            api = APILINKS.USER_DATA + SharedPref.getString(this.getContext(),"uuid","");
            parameter = jsonObject.toString();
            key = "USER_DATA";

            okHttpController = new OkHttpController(this,api,key,"GET",parameter);
            okHttpController.execute();


        }else {
            Constants.MakeToast(this.getContext(),"Please connect to internet and try again.");
        }

    }

    @Override
    public void OkHttpSuccess(String key, JSONObject result) {
        if (result != null && key != null){
            switch (key){
                case "USER_DATA":

                    progressDialogue.dismiss();
                    userDataResponse(result);
                    break;

            }
        }
    }

    @Override
    public void OkHttpFailure(String result) {

        Log.e("OkHttpFailure",""+result);
        progressDialogue.dismiss();

    }
    private void userDataResponse(JSONObject result){
        Log.e("Result",""+result);

//        Constants.MakeToast(this.getContext(),"User Data Fetched successfully.");

        JSONArray dataArray = result.optJSONArray("data");
        if (dataArray != null){
            for (int position = 0; position <dataArray.length(); position++ ){

                SharedPref.setString(this.getContext(),"user_name",dataArray.optJSONObject(position).optJSONObject("attributes").optString("Name"));
                Log.e("name","="+SharedPref.getString(this.getContext(),"user_name","Hello!"));
                nameTV.setText("Hello " + SharedPref.getString(this.getContext(),"user_name","Hello!") +"!");

            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.instagramIB:
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.instagram.com/hotelindigodubai/"));
                startActivity(intent);
                break;
            case R.id.facebookIB:
                Intent intent2 = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.facebook.com/hotelindigodubai"));
                startActivity(intent2);
                break;
            case R.id.tiktokIB:
                Intent intent3 = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.tiktok.com/@hotelindigodubai"));
                startActivity(intent3);

                break;
            case R.id.socialDotsIB:
                Log.e("SocialDots","Test");
                if (socialLL.getVisibility() == View.VISIBLE){
                    socialLL.setVisibility(View.INVISIBLE);
                }else{
                    socialLL.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.cardView:

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new ArtGalleryFragment());
                transaction.commit();

//                APILINKS.fragmentNumber = 1;

                Log.e("Card Click = ","Our art card clicked ");
                break;


            case R.id.cardView4:

                FragmentTransaction transaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                transaction1.replace(R.id.container, new RoomsFragment());
                transaction1.commit();

//                APILINKS.fragmentNumber = 1;

                Log.e("Card Click = ","Our art card clicked ");
                break;

            case R.id.cardViewHelp:

                FragmentTransaction transaction3 = getActivity().getSupportFragmentManager().beginTransaction();
                transaction3.replace(R.id.container, new HelpFragment());
                transaction3.commit();

//                APILINKS.fragmentNumber = 1;

                Log.e("Card Click = ","Our art card clicked ");
                break;

            default:
                Log.e("onClick:","Default");
        }

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent gotoArtGallery = new  Intent(HomeFragment.this.getContext(), OurStoryActivity.class);
                startActivity(gotoArtGallery);
            }
        });
    }
}