package com.example.hotelindigo.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hotelindigo.Constants.Constants;
import com.example.hotelindigo.Models.RoomsListModel;
import com.example.hotelindigo.Networks.APILINKS;
import com.example.hotelindigo.Networks.OkHttpController;
import com.example.hotelindigo.OurRoom.OurRoomModel;
import com.example.hotelindigo.OurRoom.OurRoomsAdapter;
import com.example.hotelindigo.R;
import com.example.hotelindigo.customViews.ProgressDialogue;
import com.example.hotelindigo.listeners.OkHttpListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RoomsFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RoomsFragment extends Fragment implements OkHttpListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ProgressDialogue progressDialogue;
    OkHttpController okHttpController;
    ImageView roomIV;
    TextView roomNameTV;



    //New
    RecyclerView recyclerView;
    ArrayList<OurRoomModel> list;
    ArrayList<RoomsListModel> roomsListArray;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RoomsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RoomsFragment newInstance(String param1, String param2) {
        RoomsFragment fragment = new RoomsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public RoomsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        progressDialogue = new ProgressDialogue(this.getContext());
        callRoomsAPI();


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rooms, container, false);
        recyclerView = view.findViewById(R.id.roomsRV);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));


        list = new ArrayList<>();

        OurRoomModel obj1 = new OurRoomModel(R.drawable.room, "Standard");
        list.add(obj1);

        OurRoomModel obj2 = new OurRoomModel(R.drawable.room1, "Standard 1");
        list.add(obj2);

        OurRoomModel obj3 = new OurRoomModel(R.drawable.bathroom, "Standard 2");
        list.add(obj3);

        OurRoomModel obj4 = new OurRoomModel(R.drawable.room, "Standard 3");
        list.add(obj4);

//        recyclerView.setAdapter(new OurRoomsAdapter(list));

        return view;
    }
    private void callRoomsAPI() {

        if (Constants.IsNetworkAvailable(this.getContext())){

            String api,parameter,key;

            progressDialogue.show();
            JSONObject jsonObject = new JSONObject();

            api = APILINKS.ROOMS_LIST;
            parameter = jsonObject.toString();
            key = "ROOMS_LIST";

            okHttpController = new OkHttpController(this,api,key,"GET",parameter);
            okHttpController.execute();


        }else {
            Constants.MakeToast(this.getContext(),"Please connect to internet and try again.");
        }
    }

    @Override
    public void OkHttpSuccess(String key, JSONObject result) {
        if (result != null && key != null){
            switch (key){
                case "ROOMS_LIST":

                    progressDialogue.dismiss();
                    roomListResponse(result);
                    break;

            }
        }
    }

    @Override
    public void OkHttpFailure(String result) {
        Log.e("OkHttpFailure",""+result);
        progressDialogue.dismiss();
    }



    private void roomListResponse(JSONObject result){
        Log.e("Result Rooms List",""+result);

//        Constants.MakeToast(this.getContext(),"Rooms List Fetched successfully.");

        JSONArray dataArray = result.optJSONArray("data");
        Constants.jsonArray = dataArray;

        roomsListArray = new ArrayList<>(); //model type array list


        if (dataArray != null){
            for (int position = 0; position <dataArray.length(); position++ ){

                String roomType = dataArray.optJSONObject(position).optJSONObject("attributes").optString("Type");
//                String roomImageURL = dataArray.optJSONObject(position).optJSONObject("attributes").optJSONObject("Cover").optJSONObject("data").optJSONObject("attributes").optJSONObject("formats").optJSONObject("thumbnail").optString("url");


                Log.e("imageURL=","hello");


                JSONArray roomImageArry = dataArray.optJSONObject(position).optJSONObject("attributes").optJSONObject("Images").optJSONArray("data");
                Log.e("imageURL=123","hello");

                String roomImageURL = "";


                if (roomImageArry != null){
                    roomImageURL = roomImageArry.optJSONObject(0).optJSONObject("attributes").optString("url");
                    Log.e("imageURL=",""+roomImageURL);


                }
                RoomsListModel obj = new RoomsListModel(roomImageURL,roomType);
                roomsListArray.add(obj);


                Log.e("name","="+roomType);
                Log.e("imageURLThumbnail","="+roomImageURL);
//                nameTV.setText("Hello " + SharedPref.getString(this.getContext(),"user_name","Hello!") +"!");

            }
            recyclerView.setAdapter(new OurRoomsAdapter(roomsListArray,this.getContext()));
        }


    }

}