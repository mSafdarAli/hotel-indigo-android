package com.example.hotelindigo.listeners;

import org.json.JSONObject;

public interface OkHttpListener {
    public void OkHttpSuccess(String key, JSONObject result);
    public void OkHttpFailure(String result);
}
