package com.example.hotelindigo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class HelpActivity extends AppCompatActivity implements View.OnClickListener  {

   private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_help);

        button = findViewById(R.id.help1);
        button.setOnClickListener(this);

    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.help1:
                startActivity();

                break;
            default:
                Log.e("E Tag","Hello default");

        }
    }
    @Override
    public void onBackPressed() {
        Intent intent2 = new  Intent(HelpActivity.this,BottomNavigation.class);
        startActivity(intent2);
    }
    private void startActivity() {
        Intent i = new  Intent(HelpActivity.this,Help1Activity.class);
        startActivity(i);
    }
}