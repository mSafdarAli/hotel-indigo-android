package com.example.hotelindigo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;

import com.example.hotelindigo.Fragment.ArtGalleryFragment;
import com.example.hotelindigo.Fragment.HelpFragment;
import com.example.hotelindigo.Fragment.HomeFragment;
import com.example.hotelindigo.Fragment.RoomsFragment;
import com.example.hotelindigo.Networks.APILINKS;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavigation extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bottom_navigation);

        //New
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Ok
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new HomeFragment());
        transaction.commit();

        bottomNavigationView = findViewById(R.id.buttomNavigation);
//        bottomNavigationView.setSelectedItemId(R.id.Scan);

        if (APILINKS.fragmentNumber == 1){
            Log.e("index  = 1 ","One");
        }


        bottomNavigationView.setOnNavigationItemSelectedListener (new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()){
                    case R.id.Home:
                        transaction.replace(R.id.container, new HomeFragment());
                        break;
                    case R.id.artGallery:
                        transaction.replace(R.id.container, new ArtGalleryFragment());
                        break;
                    case R.id.Scan:
//                        transaction.replace(R.id.container, new HomeFragment());
                        break;
                    case R.id.Rooms:
                        transaction.replace(R.id.container, new RoomsFragment());
                        break;
                    case R.id.Help:
                        transaction.replace(R.id.container,new HelpFragment());
                        break;
                }
                transaction.commit();
                return true;
            }
        });
    }
@Override
public void onBackPressed() {
    FragmentManager manager = getSupportFragmentManager();
    if (manager.getBackStackEntryCount() > 1 ) {
        manager.popBackStack();
    } else {
        // if there is only one entry in the backstack, show the home screen
        moveTaskToBack(true);
    }
}
}